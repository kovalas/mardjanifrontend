module.exports = function (grunt) {
    'use strict';

    grunt.initConfig({
        assemble: {
            development: {
                options: {
                    data: ['templates.json']
                },
                src: ['index.html'],
                dest: 'dist/index.html'
            }
        },
        bake: {
            development: {
                files: {
                    'dist/index.html': 'dist/index.html'
                }
            }
        },
        less: {
            development: {
                options: {
                    paths: ["vendor/bootstrap/less"]
                },
                files: {
                    "dist/admin-panel.css": "less/admin-panel.less"
                }
            }
        },
        concat: {
            js: {
                options: {
                    separator: ';'
                },
                src: [
                    'vendor/jquery/dist/jquery.js',
                    'vendor/jquery-ui/jquery-ui.js',
                    'vendor/underscore/underscore.js',
                    'vendor/bootbox/bootbox.js',
                    'vendor/bootstrap/dist/js/bootstrap.js',
                    'vendor/angular/angular.js',
                    'vendor/angular-dragdrop/src/angular-dragdrop.js',
                    'vendor/bootstrap-fileinput/js/fileinput.js',
                    'vendor/bootstrap-fileinput/js/fileinput_locale_ru.js',
                    'vendor/summernote/dist/summernote.js',
                    'js/main.js',
                    'js/controllers/mainPageController.js',
                    'js/controllers/navListController.js',
                    'js/controllers/pageEditController.js',
                    'js/controllers/linksEditController.js',
                    'js/controllers/aboutEditController.js',
                    'js/services/alerts.js',
                    'js/services/serverApi.js',
                    'js/directives/bsFileInput.js',
                    'js/directives/clickTrigger.js',
                    'js/directives/wysiwygSummernote.js',
                    'js/directives/modeSwitch.js'
                ],
                dest: 'dist/frontend.js'
            },
            css: {

                    src: [
                        'dist/admin-panel.css',
                        'vendor/font-awesome/css/font-awesome.css',
                        'vendor/bootstrap-fileinput/css/fileinput.css',
                        'vendor/summernote/dist/summernote.css'
                    ],
                    dest: 'dist/admin-panel.css'

            }
        },
        uglify: {
            production: {
                files: {
                    'dist/frontend.min.js': ['dist/frontend.js']
                }
            }
        },
        watch: {
            options: {
                livereload: true
            },
            styles: {
                files: ['less/*.less'],
                tasks: ['less:development']
            },
            code: {
                files: ['js/**/*.js'],
                tasks: ['concat']
            },
            html: {
                files: ['index.html', 'templates/**/*.html'],
                tasks: ['assemble:development', 'bake:development']
            }
        }
    });

    grunt.loadNpmTasks('grunt-contrib-concat');
    grunt.loadNpmTasks('grunt-contrib-less');
    grunt.loadNpmTasks('grunt-contrib-watch');
    grunt.loadNpmTasks('grunt-contrib-uglify');
    grunt.loadNpmTasks('grunt-bake');
    grunt.loadNpmTasks('assemble');
    grunt.registerTask('process', ['assemble:development', 'bake:development', 'less:development', 'concat:js', 'concat:css', 'uglify:production']);
    grunt.registerTask('default', ['process', 'watch']);
};
