angular.module('adminApp')
.controller('mainPageController',  ['$scope', 'serverApi', 'alerts', function($scope, serverApi, alerts) {
    var currentMode;

    serverApi.getContents()
    .success(function(summary){
        $scope.categories = summary;
    })
    .error(function () {
        alerts.error('Ошибка', 'Не удалось загрузить оглавление');
    });

    $scope.createCategory = function () {
        bootbox.prompt('Введите название нового раздела: ', function (categoryName) {
            if (categoryName != null) {
                serverApi.createCategory(categoryName)
                    .success(function (data, status, headers) {
                        $scope.categories
                            .push({
                                id: headers.newCategoryId,
                                name: categoryName,
                                texts: []
                            });
                    })
                    .error(function () {
                        alerts.error('Ошибка!', 'Не удалось добавить новый раздел');
                    });
            }
        });
    };

    $scope.editCategory= function (category) {
        bootbox.prompt('Введите название раздела: ', function (newName) {
            if (newName != null) {
                serverApi.updateCategory(category, newName)
                    .success(function () {
                        category.name = newName
                    })
                    .error(function () {
                        alerts.error('Ошибка!', 'Не удалось переименовать раздел');
                    });
            }
        });
    };

    $scope.currentModeIs = function (mode) {
        return currentMode === mode;
    };

    $scope.getCurrentMode = function () {
        return currentMode;
    };

    $scope.setLibraryMode = function () {
        currentMode = 'libraryMode';
    };

    $scope.setCurrentMode = function(mode) {
        currentMode = mode;
    };

}]);