angular.module('adminApp')
    .controller('navListController',  ['$scope', '$rootScope', 'serverApi', 'alerts', function($scope, $rootScope, serverApi, alerts){
        $scope.listOpened = false;
        $scope.droppedEntity = {};

        $rootScope.draggedCategoryId = 0;
        $scope.dragUponState = false;

        $scope.setActiveVolume = function (volume, text, category) {
            var activeVolume = {
                volume: volume,
                textId: text.id,
                categoryId: category.id
            };
            $rootScope.activeVolume = activeVolume;
            $rootScope.$broadcast('activeVolumeChanged', activeVolume);
        };

        $scope.isActiveVolume = function (volume, text, category) {
            if (!$rootScope.activeVolume) {
                return false;
            }
            return _.isEqual(
                $rootScope.activeVolume,
                {
                    volume: volume,
                    textId: text.id,
                    categoryId: category.id
                }
            );
        };

        $scope.actionDragOver = function () {
            $scope.$apply(function () {
                $scope.dragUponState = true;
            });
        };

        $scope.actionDragOut = function () {
            $scope.$apply(function () {
                $scope.dragUponState = false;
            });
        };

        $scope.actionTextDrop = function () {
            $scope.dragUponState = false;
            console.log($scope.droppedText);

            bootbox.confirm('Переместить текст' + $scope.droppedText.title + ' в категорию ' + $scope.category.name + '?', function(result) {
                console.log(result);
                serverApi.getContents();
            });
        };

        $scope.actionVolumeDrop = function () {
            $scope.dragUponState = false;
            console.log($scope.text);

            bootbox.confirm('Переместить том' + $scope.text.number + ' в категорию ' + $scope.category.name + '?', function(result) {
                console.log(result);
                serverApi.getContents();
            });
        };

        function deleteEntity(params) {
            bootbox.confirm(params.mesConfirm, function(result) {
                if (result) {
                    params.method()
                        .success(function () {
                            alerts.success('Готово!', params.mesOK);
                        })
                        .error(function (){
                            alerts.error('Ошибка!', params.mesFail);
                        });
                }
            });
        }

        $scope.createVolume = function (category, text) {
            bootbox.confirm('Добавить новый том в текст ' + $scope.text.title + ' ?', function (result) {
                if (!result) return;
                serverApi.createVolume(category, text)
                    .success(function (data, status, headers) {
                        text.volumes
                            .push({
                                number: headers('X-New-Volume-Number'),
                                pagesCount: 0
                            });
                    })
                    .error(function (){
                        alerts.error('Ошибка!', 'Не удалось добавить новый том');
                    });
            });
        };

        $scope.createText = function (category) {
            bootbox.prompt('Введите название нового текста: ', function (textName) {
                if (textName != null) {
                    serverApi.createText(category, textName)
                        .success(function (data, status, headers) {
                            category.texts.push({
                                    id: headers.newTextId,
                                    title: textName,
                                    volumes: []
                            });
                        })
                        .error(function () {
                            alerts.error('Ошибка!', 'Не удалось добавить новый текст');
                        });
                }
            });
        };

        $scope.editText = function (category, text) {
            bootbox.prompt('Введите название текста: ', function (newName) {
                if (newName != null) {
                    serverApi.updateText(category, text, newName)
                        .success(function () {
                            text.title = newName
                        })
                        .error(function () {
                            alerts.error('Ошибка!', 'Не удалось переименовать текст');
                        });
                }
            });
        };

        $scope.deleteCategory = function (category) {
            deleteEntity({
                mesConfirm: 'Удалить категорию ' + category.name + ' ?',
                mesOK: 'Категория удалена',
                mesFail: 'Не удалось удалить категорию',
                method: serverApi.deleteCategory.bind(serverApi, category)
            });
        };

        $scope.deleteText = function (category, text) {
            deleteEntity({
                mesConfirm: 'Удалить текст ' + text.title + ' ?',
                mesOK: 'Текст удален',
                mesFail: 'Не удалось удалить текст',
                method: serverApi.deleteText.bind(serverApi, category, text)
            });
        };

        $scope.deleteVolume = function (index, category, text, volume) {
            deleteEntity({
                mesConfirm: 'Удалить том ' + volume.number + ' ?',
                mesOK: 'Том удален',
                mesFail: 'Не удалось удалить том',
                method: function () {
                    text.volumes.splice(index, 1);
                    return serverApi.deleteVolume(category, text, volume);
                }
            });
        };

    }]);