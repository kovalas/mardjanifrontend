angular.module('adminApp')
    .controller('linksEditController', ['$scope', 'serverApi', 'alerts', function (scope, serverApi, alerts) {
        var linksHtml;

        scope.updateLinksHtml = function (html) {
            linksHtml = html;
        };

        scope.getInitValue = function () {
            return serverApi.getLinksPage();
        };

        scope.saveLinks = function () {
            serverApi.updateLinksPage(linksHtml)
                .success(function () {
                    alerts.success('Готово', 'Страница ссылок обновлена');
                })
                .error(function () {
                    alerts.error('Ошибка', 'Не удалось обновить страницу');
                })
        }
    }]);