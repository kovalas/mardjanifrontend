angular.module('adminApp')
    .controller('aboutEditController', ['$scope', 'serverApi', 'alerts', function (scope, serverApi, alerts) {
        var aboutHtml;

        scope.updateAboutHtml = function (html) {
            aboutHtml = html;
        };

        scope.getInitValue = function () {
            return serverApi.getAboutPage();
        };

        scope.saveAbout = function () {
            serverApi.updateAboutPage(aboutHtml)
                .success(function () {
                    alerts.success('Готово', 'Страница "О сайте" обновлена');
                })
                .error(function () {
                    alerts.error('Ошибка', 'Не удалось обновить страницу');
                })
        }
    }]);