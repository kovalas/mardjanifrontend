angular.module('adminApp')
.controller('pageEditController', ['$scope', 'serverApi', 'alerts', function ($scope, serverApi, alerts){
    $scope.page = {};

    $scope.changePage = function (page) {
        if (page.disabled) {
            return;
        }
        if (page.previous) {
            if ($scope.page.number > 1) $scope.page.number--;
            return;
        }
        if (page.next) {
            if ($scope.page.number < $scope.page.pagesCount) $scope.page.number++;
            return;
        }
        $scope.page.number=page.number;
    };

    $scope.addPage = function (page) {
        var message;
        if (!page) {
            message = " в начало тома ?";
            page = {number: 0};
        } else {
            message = " после страницы " + page.number + " ?";
        }
        bootbox.confirm("Добавить новую страницу" + message, function(result) {
            if (result) {
                serverApi.createPage(page)
                    .success(function () {
                        alerts.success('Готово', 'Новая страница добавлена');
                        $scope.page.pagesCount++;
                        loadPage();
                    })
                    .error(function () {
                        alerts.error('Ошибка', 'Не удалось добавить страницу');
                    });
            }
        });
    };

    $scope.getClass = function (page) {
        return {
            disabled: page.disabled,
            active: page.number===$scope.page.number
        }
    };
    $scope.updatePage = function () {
        serverApi.updatePage($scope.page);
    };
    $scope.deletePage = function () {
        bootbox.confirm("Удалить страницу номер " + $scope.page.number + "?", function(result) {
            if (result) {
                serverApi.deletePage($scope.page)
                    .success(function (){
                        alerts.success('Готово', 'Страница удалена');
                    })
                    .error(function () {
                        alerts.error('Ошибка', 'Не удалось удалить страницу');
                    });
            }
        });
    };

    $scope.getTextInfo = function () {
        return {
            textId: $scope.page.textId,
            volume: $scope.page.volume.number,
            page: $scope.page.number
        }
    };

    $scope.deleteImage = function (imageId) {
        bootbox.confirm("Удалить изображение?", function(result) {
            if (result) {
                serverApi.deleteImage($scope.page, imageId)
                    .success(function (){
                        delete $scope.page.images.splice($scope.page.images.indexOf(imageId), 1);
                        alerts.success('Готово', 'Изображение удалено');
                    })
                    .error(function () {
                        alerts.error('Ошибка', 'Не удалось удалить изображение');
                    });
            }
        });
    };

    $scope.$watch('page.number', function () {
        loadPage();
    });
    $scope.$on('activeVolumeChanged', function (event, data){
        _.extend($scope.page, data);
        $scope.page.number = 1;
        $scope.page.pagesCount = data.volume.pagesCount;
        loadPage();
    });

    function formatText(text) {
        return text.replace(new RegExp("<br>*", "g"), '\n');
    }

    function loadPage() {

        function shouldVisible(lastIndex, currentIndex, pageNumber) {
            return (pageNumber < 4) || (pageNumber > lastIndex - 3) || (Math.abs(pageNumber - currentIndex) < 3);
        }

        if (!_.isEmpty($scope.page)) {
            serverApi.getPage($scope.page)
                .success(function (pageResponce) {
                    $scope.page.text = formatText(pageResponce.text);
                    $scope.page.comments = pageResponce.comments;
                    $scope.page.images = pageResponce.images;
                })
                .error(function () {
                    alerts.error('Ошибка', 'Не удалось загрузить страницу');
                });

            var max = parseInt($scope.page.pagesCount);
            var current = $scope.page.number;
            var i;
            $scope.page.list = [];
            $scope.page.list.push({
                previous: true,
                disabled: $scope.page.number === 1
            });
            for (i=1; i<=max; i++) {
                if (shouldVisible(max, current, i)) {
                    $scope.page.list.push({
                        number: i
                    });
                } else {
                    $scope.page.list.push({
                        number: '...',
                        disabled: true
                    });
                    while (!shouldVisible(max, current, ++i));
                    i--;
                }
            }
            $scope.page.list.push({
                next: true,
                disabled: $scope.page.number === max
            });
        }
    }
}]);
