angular.module('adminApp')
    .directive('modeSwitch', function () {
        return {
            link: function (scope, element, attrs) {
                $(element).click(function () {
                    scope.setCurrentMode(attrs.modeSwitch);
                    scope.$apply();
                });
            }
        }
    });
