angular.module('adminApp')
    .directive('wysiwygSummernote', function () {
        return {
            link: function (scope, element, attrs) {
                $(element).summernote({
                    height: 200,
                    minHeight: null,
                    maxHeight: null,
                    focus: true,
                    onInit: init
                });

                function init() {

                    scope[attrs.init]().success(function (html) {
                       $(element).code(html);
                    });

                    $.summernote.eventHandler.attach(
                        $.summernote.core.dom.buildLayoutInfo(
                            $.summernote.renderer.layoutInfoFromHolder($(element)).editor()),
                        {
                            insertTableMaxSize: {
                                col: 3,
                                row: 3
                            },
                            onChange: function (html) {
                                scope[attrs.onchange](html);
                            }
                        }
                    )
                }

            }
        }
    });

