angular.module('adminApp')
.directive('bsFileInput', function () {
    return {
        link: function (scope, element, attrs) {
            $(element).fileinput({

                maxFileCount: 5,
                uploadAsync: true,
                overwriteInitial: false,
                allowedFileTypes: ['image'],
                uploadUrl: '/images-upload',
                dropZoneTitle: 'Перетащите файлы сюда...',
                uploadExtraData: scope.getTextInfo
            });
        }
    }
});
