angular.module('adminApp')
    .directive('clickTrigger', function () {
        return {
            link: function (scope, element, attrs) {
                $(element).on('click', function () {
                    scope[attrs.clickTrigger]();
                    scope.$apply();
                });
            }
        }
    });
