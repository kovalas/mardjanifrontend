angular.module('adminApp')
.service('alerts', ['$rootScope', '$timeout', function (rootScope, timeout) {
    function showAlert(parameters) {
        rootScope.alert = parameters;
        timeout(function () {
            rootScope.alert = null;
        }, 3000);
    }

    this.success = function (caption, text) {
        showAlert({
            type: 'alert-success',
            caption: caption,
            text: text
        });
    };

    this.error = function (caption, text) {
        showAlert({
            type: 'alert-danger',
            caption: caption,
            text: text
        });
    };
}
]);
