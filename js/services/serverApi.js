angular.module('adminApp')
.service('serverApi', ['$q', '$http', function(q, http){

    var apiUrl="/libraryApi/";

    function getUrlByPagePath(pagePath) {
        return apiUrl +
            "category/" + pagePath.categoryId +
            "/text/" + pagePath.textId +
            "/volume/" + pagePath.volume.number +
            "/page/" + pagePath.number;
    }

    this.getPage = function (pagePath) {
        return http.get(getUrlByPagePath(pagePath));
    };

    this.updatePage = function (page) {
        return http.put(getUrlByPagePath(page), page);
    };

    this.deletePage = function (page) {
        return http.delete(getUrlByPagePath(page));
    };

    this.deleteImage = function (page, imageId) {
        return http.delete(getUrlByPagePath(page) + '/image/' + imageId);
    };

    function getPathForCategory(category) {
        return apiUrl + 'category/' + category.id;
    }

    function getPathForText(categoty, text) {
        return getPathForCategory(categoty) + '/text/' + text.id;
    }

    this.deleteCategory = function (category) {
        return http.delete(getPathForCategory(category));
    };

    this.deleteText = function (category, text) {
        return http.delete(getPathForText(category, text));
    };

    this.deleteVolume = function (category, text, volume) {
        return http.delete(getPathForText(category, text) + '/volume/' + volume.number);
    };

    this.getContents = function () {
        var url = apiUrl + 'contents';
        return http.get(url);
    };

    this.createVolume = function (category, text) {
        return http.post(getPathForText(category, text) + '/volumes', {});
    };

    this.createText = function (category, textName) {
        return http.post(getPathForCategory(category) + '/texts', {
            name: textName
        });
    };

    this.updateText = function (category, text, newTitle) {
        return http.put(getPathForText(category, text), {
            title: newTitle
        });
    };

    this.createCategory = function (categoryName) {
        return http.post(apiUrl + 'categories', {
            name: categoryName
        });
    };

    this.updateCategory = function (category, newName) {
        return http.put(getPathForCategory(category), {
            name: newName
        });
    };

    this.createPage = function (page) {
        var pageNum = page.number + 1,
            url = apiUrl + "category/" + page.categoryId +
            "/text/" + page.textId +
            "/volume/" + page.volume.number +
            "/page/" + pageNum;
        return http.post(url, {
            number: page.number
        });
    };

    this.getLinksPage = function () {
        return http.get("/contentApi/links");
    };

    this.updateLinksPage = function (html) {
        return http.put("/contentApi/links", html);
    };

    this.getAboutPage = function () {
        return http.get("/contentApi/about");
    };

    this.updateAboutPage = function (html) {
        return http.put("/contentApi/about", html);
    };

 }]);
